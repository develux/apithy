<?php

namespace App\Http\Controllers;

use App\Rol;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $rules = [
        'file' => 'required|mimes:xlsx'
    ];

    private $rulesExcel = [
        'name' => 'required|max:50',
        'email' => 'required|email|unique:users,email',
        'code' => 'exists:roles,code'
    ];

    public function index(Request $request)
    {
        try {
            if ($request->type == "valid") {
                $file = file_get_contents(Storage::path('uploads/valid.xlsx'));
            } else if ($request->type == "invalid") {
                $file = file_get_contents(Storage::path('uploads/invalid.xlsx'));

            } else {
                throw new \Exception("Invalid type");
            }

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header("Content-Disposition: attachment;filename=$request->type.xlsx");
            header('Access-Control-Allow-Origin: *');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0
            echo $file;
        } catch (\Exception $e) {
            return response()->json(["errors" => $e->getMessage()]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        set_time_limit(0);
        try {
            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->getMessageBag()->toArray()]);
            }

            $uuid = uniqid();
            $file = $request->file('file');

            Storage::put("uploads/users/good/$uuid.xlsx", $file->get());
            $Spreadsheet = IOFactory::load(Storage::path("uploads/users/good/$uuid.xlsx"));
            $xls_data = $Spreadsheet->getActiveSheet()->toArray(null, true, true, true);

            $spreadsheetInvalid = new Spreadsheet();
            $spreadsheetInvalid->setActiveSheetIndex(0)
                ->setCellValue('A1', 'name')
                ->setCellValue('B1', 'email')
                ->setCellValue('C1', 'code')
                ->setCellValue('D1', 'error');

            $spreadsheetValid = new Spreadsheet();
            $spreadsheetValid->setActiveSheetIndex(0)
                ->setCellValue('A1', 'name')
                ->setCellValue('B1', 'email')
                ->setCellValue('C1', 'code');
            $totalInvalid = 0;
            $totalValid = 0;
            $iValid = 2;
            $iInvalid = 2;

            $date = date("Y-m-d H:i:s");
            for ($i = 2; $i <= count($xls_data); $i++) {
                $name = $Spreadsheet->getActiveSheet()->getCell("A$i");
                $email = $Spreadsheet->getActiveSheet()->getCell("B$i");
                $code = $Spreadsheet->getActiveSheet()->getCell("C$i");
                $data["name"] = $name;
                $data["email"] = $email;
                $data["code"] = $code;
                $validatorExcel = Validator::make($data, $this->rulesExcel);
                if ($validatorExcel->fails()) {
                    $totalInvalid++;
                    $spreadsheetInvalid->setActiveSheetIndex(0)
                        ->setCellValue("A$iInvalid", $name)
                        ->setCellValue("B$iInvalid", $email)
                        ->setCellValue("C$iInvalid", $code)
                        ->setCellValue("D$iInvalid", $validatorExcel->errors()->first());
                    $iInvalid++;
                } else {
                    $rol = Rol::where('code', '=', $code)->first();
                    $user = new User();
                    $user->name = $name;
                    $user->email = $email;
                    $user->password = Hash::make(rand());
                    $user->role_id = $rol->id;
                    $user->created_at = date("Y-m-d H:i:s");
                    $user->updated_at = date("Y-m-d H:i:s");
                    $user->save();
                    $totalValid++;
                    $spreadsheetValid->setActiveSheetIndex(0)
                        ->setCellValue("A$iValid", $name)
                        ->setCellValue("B$iValid", $email)
                        ->setCellValue("C$iValid", $code);
                    $iValid++;
                }
            }

            $spreadsheetInvalid->getActiveSheet()->setTitle('Invalid users');
            $writer = new Xlsx($spreadsheetInvalid);
            $fxls = Storage::path("uploads/invalid.xlsx");
            $writer->save($fxls);

            $spreadsheetValid->getActiveSheet()->setTitle('Valid users');
            $writerValid = new Xlsx($spreadsheetValid);
            $fxlsValid = Storage::path("uploads/valid.xlsx");
            $writerValid->save($fxlsValid);
            return response()->json(["success" => "Has been created", "valid" => $totalValid, "invalid" => $totalInvalid]);
        } catch (\Exception $e) {
            return response()->json(["errors" => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id == 0) {
            $users = User::with('rol')->limit(50)->get();
            return response()->json($users);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
