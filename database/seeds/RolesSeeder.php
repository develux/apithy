<?php

use Illuminate\Database\Seeder;
use App\Rol;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $roles = [
        [
            "name" => "Administrador",
            "code" => "AD",
        ],
        [
            "name" => "Profesor",
            "code" => "PROF",
        ],
        [
            "name" => "Estudiante",
            "code" => "STU"
        ],
    ];

    public function run()
    {
        foreach ($this->roles as $rol) {
            Rol::create($rol);
        }
    }
}
